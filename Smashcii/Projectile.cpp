#include "Projectile.h"


Projectile::Projectile()
{
	charInfo->Char.AsciiChar = static_cast<wchar_t>(61);
	charInfo->Attributes = FOREGROUND_BLUE | FOREGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY;
	disposable = true;
}

Projectile::~Projectile()
{
}

void Projectile::update()
{
	origin.X += direction.X;
	origin.Y += direction.Y;
}
