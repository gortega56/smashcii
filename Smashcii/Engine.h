
#include <Windows.h>
#include <vector>
#include <set>
#pragma once

using namespace std;

const int WINDOW_WIDTH = 70;
const int WINDOW_HEIGHT = 35;

class View;
class Projectile;
class Graph;
class Enemy1;

class Engine
{
	
public:
	SMALL_RECT			windowRect, writeRect;
	HANDLE				outputHandle;
	CHAR_INFO*			charBuffer;

	vector <Projectile*>	projectiles;
	vector <Enemy1 *>		enemies;
	vector <View*>			subviews;
	set<PCOORD>			enemySpawn;
	Graph*				graph;
	View*				player;
	COORD				playerSpawn;
	int					killCount;
	bool				gameOver;

	Engine();
	~Engine();
	
	CHAR_INFO* charInfoForGraph(Graph *graph);
	void drawGraph(Graph* graph);
	void gameLoop(Graph *graph);

	void updateViewPositions();
	void movePlayerToPosition(int x, int y);
	void shootProjectile(COORD origin, COORD direction);
	
	bool disposeView(View* view);
	void processEventType(INPUT_RECORD inputRecord);
};

