#include "AStar.h"
#include "Vertex.h"
#include <iostream>
#include <algorithm>
#include <math.h>
#include <vector>

using namespace std;


int getHeuristicForVertex(Vertex *source, Vertex *destination);
int getMovementCost(Vertex *source, Vertex* destination)
{
	return sqrt(pow((destination->y - source->y), 2) + pow((destination->x - source->x), 2)) * 10;
}

string Node::toString()
{
	return vertex->toString();
}

AStar::AStar()
{

}


AStar::~AStar()
{
}

Node* AStar::findOrCreateNodeByVertex(Vertex *vertex)
{
	Node *nodePointer = __nullptr;
	for (Node *node : openNodeSet) {
		if (vertex->identifier.compare(node->vertex->identifier) == 0) {
			nodePointer = node;
		}
	}

	if (nodePointer == __nullptr) {
		nodePointer = new Node(vertex);
		if ((CATEGORY)(vertex->category) != WALL) {
			openNodeSet.insert(nodePointer);
		}
		else {
			nodePointer->isObstacle = true;
			closedNodeSet.insert(nodePointer);
		}
	}



	return nodePointer;
}

stack<Node*> AStar::findPath(Vertex* const source, Vertex* const destination)
{
	openNodeSet.clear();
	closedNodeSet.clear();

	addOpenNodeForVertex(source);
	getNextNode();

	while ((currentNode->vertex->identifier.compare(destination->identifier) != 0) && !openNodeSet.empty()) {
				//	cout << "Current Node:" << currentNode->vertex->toString() << endl;
		// Get Adjacent Vertices...
		set<Vertex*> adjacentVertices = currentNode->vertex->edgeVertices();
		set<Vertex*> cv = closedVertices();

		for (Vertex *vertex : adjacentVertices){
			
			bool isClosed = (cv.find(vertex) != cv.end());
			if (isClosed) {
				continue;
			}

			// Update FGH for all adjacent Vertices
			// F = G + H
			// G = movement cost from current Vertex 
			// H = total distance from sqare to destination
			// Add adjacent vertices to open list
			Node *node = findOrCreateNodeByVertex(vertex);
		//	cout << "Child Node:" << node->vertex->toString() << endl;

			if (node->parent == __nullptr) {
				node->parent = const_cast<Node *>(currentNode);

				node->G = currentNode->G + getMovementCost(currentNode->vertex, node->vertex);
				node->H = getHeuristicForVertex(vertex, destination);
				node->F = node->G + node->H;
			}
			else {
				// if not on open list add current square as parent
				// else compare G and update parent if better path
				int tempG = node->G + currentNode->G;
				if (currentNode->G + getMovementCost(currentNode->vertex, node->vertex) < node->G) {
					node->parent = const_cast<Node *>(currentNode);
					node->G = currentNode->G + getMovementCost(currentNode->vertex, node->vertex);
					node->F = node->G + node->H;
				}
			}

		}

		//cout << "Closed Nodes: " << endl;
		closedNodeSet.insert(const_cast<Node *>(currentNode));
	//	for (Node *node : closedNodeSet) {
	//		cout << node->vertex->toString() << endl;
	//	}

		openNodeSet.erase(const_cast<Node *>(currentNode));
		if (openNodeSet.empty()) {
			cout << "PATH NOT FOUND";
			break;
		}

	//	cout << "Open Nodes: " << endl;
	//	for (Node *node : openNodeSet) {
	//		cout << node->vertex->toString() << endl;
	//	}


		// Get Lowest Fscore Vertex from openList
		getNextNode();
	}

	return outputPath();
}

stack<Node*> AStar::outputPath()
{
	stack<Node *> nodeStack;
	while (currentNode->parent != __nullptr) {
		nodeStack.push(const_cast<Node*>(currentNode));
//		cout << currentNode->vertex->toString() << " - ";
		currentNode = currentNode->parent;
	}

	nodeStack.push(const_cast<Node*>(currentNode));

//	cout << currentNode->vertex->toString();
	return nodeStack;
}

void AStar::addOpenNodeForVertex(Vertex *vertex)
{
	Node *node = new Node(vertex);
	openNodeSet.insert(node);
}

bool nodeSort(Node *node1, Node *node2) {
	return (node1->F < node2->F);
}

void AStar::getNextNode()
{

	vector<Node *> sortedNodes;
	for (Node *n : openNodeSet) {
		sortedNodes.push_back(n);
	}

	sort(sortedNodes.begin(), sortedNodes.end(), nodeSort);
	Node* node = *sortedNodes.begin();
	
	//cout << "Retrieved Node: " << node->vertex->toString()  << endl;
	currentNode = node;
}

int getHeuristicForVertex(Vertex *source, Vertex *destination)
{
	int x = abs(destination->x - source->x);
	int y = abs(destination->y - source->y);

	return (x + y) * 10;
	//if (x > y) {
	//	return 1.4 * y + (x - y);
	//}
	//else {
	//	return 1.4 * x + (y - x);
	//}

}

set<Vertex *> AStar::closedVertices()
{
	set<Vertex *> closedVertices;
	for (Node *node : closedNodeSet) {
		closedVertices.insert(node->vertex);
	}

	return closedVertices;
}

