#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

// Functions

/*Read the Game's Level Width and Height from a Text File. Level's first line must contain its Height and Width in that order separated by one space. Information is stored during Heap
Input: 
	Level File: The Game Text Level
Output:
	A pointer to a Integer Array containing [Level Width, Level Height]
*/
int * GetLevelWidthHeight(std::string LevelFile);

/*Read the Game's Level. It needs to know the level's Height. Information is stored during Heap.
Input:
Level File: The Game Text Level
Output:
Integer Array containing [Level Width, Level Height]
*/
std::vector<std::string> ReadLevel(std::string LevelFile);