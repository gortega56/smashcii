#include "Graph.h"
#include "Vertex.h"
#include <math.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include "ReadLevel.h"


using namespace std;

Vertex * findNeighborsForVertex(Vertex *vertex);

// Default Constructor
Graph::Graph() : vertexCount(0)
{
}

// Constructor for a given size
Graph::Graph(int width, int height) : vertexCount(0), width(width), height(height)
{
	vertices = new Vertex[width * height];
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++){
			//vertices[height * y + x] = Vertex();
			vertices[height * x + y].identifier = "(" + to_string(x) + ", " + to_string(y) + ")";
			vertices[height * x + y].x = x;
			vertices[height * x + y].y = y;
			vertexCount++;
			//cout << vertices[height * x + y].toString() << endl;
		}
	}

	for (int index = 0; index < vertexCount; index++) {

		Vertex* vertex = &vertices[index];


		int nRow = vertex->x - 1;
		for (int row = 0; row < 3; row++) {

			int nColumn = vertex->y - 1;
			for (int column = 0; column < 3; column++) {

				if ((nRow >= 0 && nRow < width && nColumn >= 0 && nColumn < height) && !(nRow == vertex->x && nColumn == vertex->y)) {
					vertex->addAdjacentVertex(vertices[height * nRow + nColumn]);
					//cout << vertex->toString() << " ADDS " << vertices[height * nRow + nColumn].toString() << endl;
				}
				else {
					//cout << vertex->toString() << " DOES NOT ADD (" << nRow << ", " << nColumn << ")" << endl;
				}
				nColumn++;
			}
			nRow++;
		}
	}
}

// Contructor for Lvl From File
Graph::Graph(std::string LvlFile) : vertexCount(0), width(width), height(height)
{
	int * ptWidthHeight; // Int Array pointer to get Width and Height from File
	std::vector<string> LvlVector;

	/***********************************/
	//Get Width and Height from file
	ptWidthHeight = GetLevelWidthHeight(LvlFile);	// Reads file
	width = ptWidthHeight[0];						// Width = position 0
	height = ptWidthHeight[1];						// Height = position 1
	/***********************************/

	LvlVector = ReadLevel(LvlFile);

	/***********************************/
	// Constructs Graph:

	vertices = new Vertex[width * height];


	vector<string>::iterator LvlIterator = LvlVector.begin();
	std::string TempString;

	// height * x + y  => width * y + x
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			//vertices[height * y + x] = Vertex();
			vertices[width * y + x].identifier = "(" + to_string(x) + ", " + to_string(y) + ")";
			vertices[width * y + x].x = x;
			vertices[width * y + x].y = y;
			
			//LvlIterator = LvlVector.begin();
			TempString = *(LvlIterator);
			
			vertices[width * y + x].LvlChar = TempString.at(x);
			//cout << vertices[height * x + y].LvlChar << endl;
			
			if (vertices[width * y + x].LvlChar == '#')
			{
				vertices[width * y + x].category = WALL;
			}
			else if (vertices[width * y + x].LvlChar == '&') 
			{
				vertices[width * y + x].category = PLAYER_SPAWN;
			}
			else if (vertices[width * y + x].LvlChar == '@') 
			{
				vertices[width * y + x].category = ENEMY_SPAWN;
			}
			vertexCount++;
			//LvlIterator++;
			//cout << vertices[width * y + x].toString() << endl;
		}
		
		LvlIterator++;
		//LvlIterator = LvlVector.begin();
	}

	for (int index = 0; index < vertexCount; index++) {

		Vertex* vertex = &vertices[index];

		int nRow = vertex->x - 1;
		for (int row = 0; row < 3; row++) {
			
			int nColumn = vertex->y - 1;
			for (int column = 0; column < 3; column++) {

				// height * nRow + nColumn => width *nColumn + nRow
				if ((nRow >= 0 && nRow < width && nColumn >= 0 && nColumn < height) && !(nRow == vertex->x && nColumn == vertex->y)) {
					vertex->addAdjacentVertex(vertices[width *nColumn + nRow]);
					//cout << vertex->toString() << " ADDS " << vertices[height * nRow + nColumn].toString() << endl;
				}
				else {
					//cout << vertex->toString() << " DOES NOT ADD (" << nRow << ", " << nColumn << ")" << endl;
				}
				nColumn++;
			}
			nRow++;
		}
	}

	for (int index = 0; index < vertexCount; index++) {
		//cout << vertices[index].toString() << " Neighors: "<< vertices[index].adjacentVertexCount << endl;
	}
}

Graph::~Graph() 
{
}

void Graph::addVertex(Vertex& vertex)
{
	vertices[vertexCount] = vertex;
	vertexCount++;
}

string Graph::toString()
{
	//string result = "VERTICES:\n";
	//for (int index = 0; index < vertexCount; index++) {
	//	result.append("VERTEX:\n");
	//	result.append(vertices[index].toString() + "\n");

	//	if (vertices[index].adjacentVertexCount > 0) {
	//		result.append("NEIGHBORS:\n");
	//		for (Vertex* aVertex : vertices[index].adjacentVertices) {
	//			result.append(aVertex->toString() + "\n");
	//		}
	//	}
	//}

	string result = "VERTICES:\n";

	for (int y = 0; y < height; y++)
	{
		
		for (int x = 0; x < width; x++){
			result.append("VERTEX:\n");
			result.append(vertices[y * height + x].toString() + "\n");
			if (vertices[y * height + x].adjacentVertexCount > 0) {
				result.append("NEIGHBORS:\n");
				for (Vertex* aVertex : vertices[y * height + x].adjacentVertices) {
					result.append(aVertex->toString() + "\n");
				}
			}
		}
	}

	return result;
}