// Vertex.h
#pragma once
#include <string>
#include <set>
#include <Windows.h>

using namespace std;

enum CATEGORY {WALL, FLOOR, WATER, ENEMY_SPAWN, PLAYER_SPAWN};

class Vertex
{

public:

	set< Vertex*> adjacentVertices;


	string identifier;
	int x, y;
	int adjacentVertexCount;
	char LvlChar;
	CATEGORY category;

	Vertex();
	Vertex(string identifier);
	Vertex(const Vertex& vertex);
	~Vertex();

	void addAdjacentVertex(Vertex& vertex);
	set<Vertex *> edgeVertices();

	string toString();
	CHAR_INFO charInfo();

	friend bool operator==(const Vertex& leftVertex, const Vertex& rightVertex);
};
