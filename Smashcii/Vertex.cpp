#include "Vertex.h"
#include <iostream>

using namespace std;



Vertex::Vertex() : adjacentVertexCount(0), category(FLOOR)
{

}

Vertex::Vertex(string identifier) : identifier(identifier), adjacentVertexCount(0), category(FLOOR)
{

}

Vertex::Vertex(const Vertex& vertex)
{
	identifier = vertex.identifier;
	category = vertex.category;
	adjacentVertexCount = 0;
	LvlChar = ' ';

	for (Vertex *adjacentVertex : vertex.adjacentVertices) {
		addAdjacentVertex(static_cast<Vertex>(vertex));
	}
}

Vertex::~Vertex()
{

}

void Vertex::addAdjacentVertex(Vertex& vertex)
{
	adjacentVertices.insert(&vertex);
	adjacentVertexCount++;
}

string Vertex::toString()
{
	if (identifier.length() == 0) {
		return "";
	}

	return identifier;
}

set<Vertex *> Vertex::edgeVertices()
{
	return adjacentVertices;
}

CHAR_INFO Vertex::charInfo()
{
	CHAR_INFO charInfo;
	switch (category) {
		case WALL:
		{
			charInfo.Char.AsciiChar = static_cast<wchar_t>(35);
			charInfo.Attributes = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED;
			break;
		}
		case ENEMY_SPAWN:
		case PLAYER_SPAWN:
		case FLOOR:
		{
			// space
			charInfo.Char.AsciiChar = static_cast<wchar_t>(32);
			charInfo.Attributes = BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
			break;
		}
		case WATER:
		{
			// underscore
			charInfo.Char.AsciiChar = static_cast<wchar_t>(95);
			charInfo.Attributes = FOREGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY;
			break;
		}
		default:
		{
			// space
			charInfo.Char.AsciiChar = static_cast<wchar_t>(32);
			charInfo.Attributes = BACKGROUND_GREEN;
			break;
		}
	}

	
	return charInfo;
}

bool operator==(const Vertex& leftVertex, const Vertex& rightVertex)
{
	return (leftVertex.x == rightVertex.x && leftVertex.y == rightVertex.y);
}