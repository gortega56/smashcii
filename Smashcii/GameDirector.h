#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include "Engine.h"
#include "Graph.h"
#include <ctime>

using namespace std;

class Engine;

class Graph;

class GameDirector 
{
	public:
		Graph*	CurrentLevel;	// Graph with current Level
		Engine	GameEngine;		// Game engine
		int		Score;			// Player's Score
		int		TotalLevels;	// Total number of levels avaiable

		GameDirector();
		~GameDirector();
		GameDirector(const int LvlTotal);
		
		// Starts the engine loop
		void StartGameLoop();
		// Loads a Ramdom Level based on total lvls
		void GameDirector::LoadRandomLvl();
		// Loads a specific Level based on total lvl
		void GameDirector::LoadLvl(int LvlNumber);
};