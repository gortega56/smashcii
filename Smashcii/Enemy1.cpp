#pragma once
#include "Enemy1.h"
#include "Engine.h"
#include "Vertex.h"
#include "Graph.h"
#include <vector>
#include <iostream>

using namespace std;

Enemy1::Enemy1()
{
}

//Pass the engine address and spawn point as COORD during initialization
//We are passing the engine because engine has the graph and the player coordinate at the same time
Enemy1::Enemy1(Engine * engine, COORD start) : _engine(engine)
{
	origin = start;
	_destination = _engine->player;
	charInfo->Char.AsciiChar = static_cast<wchar_t>(127);
	charInfo->Attributes = FOREGROUND_RED| FOREGROUND_GREEN |BACKGROUND_BLUE| FOREGROUND_INTENSITY;
	
	int width = _engine->graph->width;
	int height = _engine->graph->height;

	//Creating a Copy of the Vertices to enusre better performance
	_vertices = new Vertex[width * height];
	_height = height;
	_width = width;
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++){
			int index = width * y + x;
			_vertices[index].identifier = _engine->graph->vertices[index].identifier;
			_vertices[index].x = _engine->graph->vertices[index].x;
			_vertices[index].y = _engine->graph->vertices[index].y;
			_vertices[index].category = _engine->graph->vertices[index].category;
			_vertices[index].adjacentVertices = _engine->graph->vertices[index].edgeVertices();
			_vertices[index].adjacentVertexCount = _engine->graph->vertices[index].adjacentVertexCount;
		}
	}
}

Enemy1::~Enemy1()
{
}

void Enemy1::update()
{
	//store the previous position of the player 
	View* originalPos = _destination;
	_destination = _engine->player;
	
	Vertex *source, *destination;
	

	//get the Source and Destination in terms of the Vertex
	destination = &_vertices[(_destination->origin.Y * _width) + _destination->origin.X];

	source = &_vertices[(origin.Y * _width) + origin.X];

	//If the player has moved more than 2 positions in x or y direction, re-compute the path
	if ((abs(_destination->origin.X - originalPos->origin.X) > 2) || (abs(_destination->origin.Y - originalPos->origin.Y) > 2) || _path.empty())
	{
		//std::cout << "DOOING STUFF" << endl;
		_path = _pathFindingAlgorithm.findPath(source, destination);
	}

	Node * nextStep = _path.top();
	_path.pop();
	//Setting the position for next iteration
	origin.X = nextStep->vertex->x;
	origin.Y = nextStep->vertex->y;
}


//Same function as above but the here the Engine can pass the player's position instead for the enemies reaching in to grab it
void Enemy1::update(View * playerPos)
{
	//store the previous position of the player 
	View* originalPos = _destination;
	_destination = playerPos;

	Vertex *source, *destination;


	//get the Source and Destination in terms of the Vertex
	destination = &_vertices[(_destination->origin.Y * _width) + _destination->origin.X];

	source = &_vertices[(origin.Y * _width) + origin.X];

	//If the player has moved more than 2 positions in x or y direction, re-compute the path
	if ((abs(_destination->origin.X - originalPos->origin.X) > 4) || (abs(_destination->origin.Y - originalPos->origin.Y) > 4))
	{
		_path = _pathFindingAlgorithm.findPath(source, destination);
	}

	Node * nextStep = _path.top();
	_path.pop();
	origin.X = nextStep->vertex->x;
	origin.Y = nextStep->vertex->y;
}