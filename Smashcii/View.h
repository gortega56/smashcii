#pragma once

#include <Windows.h>

class View
{
public:
	View(COORD origin, COORD size);
	View();
	~View();

	COORD origin, size, direction;
	CHAR_INFO* charInfo;
	bool disposable, destroy;

	virtual void update();
};

