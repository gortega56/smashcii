#include "GameDirector.h"


//Constructors
GameDirector::GameDirector()
{
	Score = 0;
	TotalLevels = 3;
	CurrentLevel = new Graph("Levels\\01.txt");
	GameEngine = Engine();
	GameEngine.graph = CurrentLevel;
	
}

GameDirector::GameDirector( const int LvlTotal)
{
	Score = 0;
	TotalLevels = LvlTotal;
	//CurrentLevel = new Graph("Levels\\01.txt");
	GameEngine = *(new Engine());
	//GameEngine.graph = CurrentLevel;

}

//Desonstructors
GameDirector::~GameDirector()
{

}

void GameDirector::StartGameLoop()
{
	GameEngine.gameLoop(CurrentLevel);

	Score = GameEngine.killCount;
}

void GameDirector::LoadRandomLvl()
{
	// Seeds random number generator
	srand(time(NULL));
	
	//Builds  Lvl to be loaded:
	string LvlToLoad = "Levels\\0";
	stringstream Number;
	Number << rand() % TotalLevels + 1;
	LvlToLoad += Number.str();
	LvlToLoad += ".txt";

	// Loads Level
	CurrentLevel = new Graph(LvlToLoad);
	GameEngine.graph = CurrentLevel;

}

void GameDirector::LoadLvl(int LvlNumber)
{
	//Builds  Lvl to be loaded:
	string LvlToLoad = "Levels\\0";
	stringstream Number;
	Number << LvlNumber;
	LvlToLoad += Number.str();
	LvlToLoad += ".txt";

	// Loads Level
	CurrentLevel = new Graph(LvlToLoad);
	GameEngine.graph = CurrentLevel;

}