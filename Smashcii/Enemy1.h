#pragma once
#include "View.h"
#include "AStar.h"
#include <stack>

class Vertex;
class Graph;
class Engine;

class Enemy1 : public View
{
public:
	Enemy1();
	Enemy1(Engine *, COORD);
	~Enemy1();
	void update();
	void update(View *);
private:
	AStar _pathFindingAlgorithm;
	int _height, _width;
	View *_destination;
	View *_player;
	Engine *_engine;
	Vertex* _vertices;
	stack<Node *> _path;
};
