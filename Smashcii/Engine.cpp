#pragma once
#include "Engine.h"
#include "Graph.h"
#include "Vertex.h"
#include <stack>
#include <iostream>
#include "View.h"
#include "Projectile.h"
#include <algorithm>
#include "Enemy1.h"
#include <mmsystem.h>
#include <future>

using namespace std;

const float frameInterval = 80; // 60FPS 

bool projectileInBounds(SMALL_RECT window, const Projectile& projectile);

const int CONST_KILL_COUNT = 30;

Engine::Engine()
{
	/*windowRect = { 0, 0, WINDOW_WIDTH - 1, WINDOW_HEIGHT - 1 };
	writeRect = { 0, 0, WINDOW_WIDTH - 1, WINDOW_HEIGHT - 1 };*/
}

Engine::~Engine()
{
	delete player;
	delete charBuffer;

	enemySpawn.clear();
	subviews.clear();
	player = __nullptr;
	charBuffer = __nullptr;
}

CHAR_INFO* Engine::charInfoForGraph(Graph *graph)
{
	for (int index = 0; index < graph->vertexCount; index++) {		
		charBuffer[index] = graph->vertices[index].charInfo();
	}

	return charBuffer;
}

void updateCharBufferWithView(SMALL_RECT& window, CHAR_INFO* charBuffer, View& view)
{
	for (int xOffset = 0; xOffset < view.size.X; xOffset++) {
		for (int yOffset = 0; yOffset < view.size.Y; yOffset++) {
			charBuffer[(window.Right + 1) * (view.origin.Y + yOffset) + (view.origin.X + xOffset)] = view.charInfo[view.size.X * yOffset + xOffset];
		}
	}
}

bool viewsIntersect(const View& view1, const View& view2)
{
	return (!(view1.origin.X + view1.size.X < view2.origin.X)	&&		// left
			!(view1.origin.X  > view2.origin.X + view2.size.X)	&&		// right
			!(view1.origin.Y + view1.size.Y < view2.origin.Y)	&&		// top
			!(view1.origin.Y > view2.origin.Y + view2.size.Y));			// bottom
}

bool Engine::disposeView(View* view) 

{
	return ((!(view->origin.X >= 0 && view->origin.X <= windowRect.Right && view->origin.Y >= 0 && view->origin.Y <= windowRect.Bottom) || 
			((graph->vertices[(view->origin.Y * graph->width) + view->origin.X]).category == WALL)));
}

void Engine::updateViewPositions()
{
	// Reset charbuffer to default graph values
	for (int index = 0; index < graph->vertexCount; index++) {
		charBuffer[index] = graph->vertices[index].charInfo();
	}

	set<View*> viewsToDestroy;
	set<View*> viewsToUpdate;
	viewsToUpdate.insert(player);

	int enemiesToRespawn = 0;

	// Detect Player/Enemy Collisions
	for (std::vector<Enemy1*>::iterator enemy = enemies.begin(); enemy != enemies.end(); enemy++) {
		
		if (viewsIntersect(**enemy, *player)) {
			(*enemy)->destroy = true;
			player->destroy = true;
			gameOver = true;
			continue;
		}

		// Enemy / Projectile Collisions
		for (std::vector<Projectile*>::iterator projectile = projectiles.begin(); projectile != projectiles.end(); projectile++) {
			if (viewsIntersect(**enemy, **projectile)) {
				(*enemy)->destroy = true;
				(*projectile)->destroy = true;
				enemiesToRespawn++;
				continue;
			}

			// Projectile / Bounds / Wall Collisions
			if (disposeView(*projectile)) {
				(*projectile)->destroy = true;
				continue;
			}

			viewsToUpdate.insert(*projectile);
		}

		viewsToUpdate.insert(*enemy);
	}

	enemies.erase(remove_if(enemies.begin(), enemies.end(), [](Enemy1* view){ return view->destroy; }), enemies.end());
	projectiles.erase(remove_if(projectiles.begin(), projectiles.end(), [](View* view){ return view->destroy; }), projectiles.end());
	subviews.erase(remove_if(subviews.begin(), subviews.end(), [](View* view){ return view->destroy; }), subviews.end());

	for (int spawnCount = 0; spawnCount < enemiesToRespawn; spawnCount++) {
		killCount++;
		int i = rand() % enemySpawn.size();
		int j = 0;
		std::set<PCOORD>::iterator vIterator = enemySpawn.begin();
		for (; j < i; vIterator++, j++)
		{
		}

		Enemy1 *enemyA = new Enemy1(this, **vIterator);
		enemies.push_back(enemyA);
	}
	
	if (killCount > CONST_KILL_COUNT) {
		gameOver = true;
	}


	vector<future<void>> updates;
	// Update charBuffer with subviews
	for (std::set<View*>::iterator view = viewsToUpdate.begin(); view != viewsToUpdate.end(); view++) {
		//(*view)->update();
		updates.push_back(async([this, view] {(*view)->update(); }));
	}

	for (auto &update : updates) {
		update.get();
	}

	for (std::set<View*>::iterator view = viewsToUpdate.begin(); view != viewsToUpdate.end(); view++) {
		updateCharBufferWithView(windowRect, charBuffer, **view);
	}

	//for (std::set<View*>::iterator view = subviews.begin(); view != subviews.end(); view++) {
	//	(*view)->update();
	//	updateCharBufferWithView(windowRect, charBuffer, **view);
	//}
}

void Engine::drawGraph(Graph* graph)
{
	windowRect = { 0, 0, graph->width - 1, graph->height - 1 };
	writeRect = { 0, 0, graph->width - 1, graph->height - 1 };

	updateViewPositions();

	COORD coordinateBufferSize, coordinateBufferCoordinate;
	BOOL success;

	outputHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	if (outputHandle == INVALID_HANDLE_VALUE){
		cout << "OUTPUT HANDLE FAILED" << endl;
		return;
	}

	coordinateBufferSize.X = graph->width;
	coordinateBufferSize.Y = graph->height;

	coordinateBufferCoordinate.X = 0;
	coordinateBufferCoordinate.Y = 0;

//	system("mode 239, 82");
	ShowWindow(GetConsoleWindow(), SW_MAXIMIZE);

	SetConsoleWindowInfo(outputHandle, TRUE, &windowRect);
	SetConsoleScreenBufferSize(outputHandle, coordinateBufferSize);

	success = WriteConsoleOutputA(
		outputHandle,					// screen buffer to write to 
		charBuffer,						// buffer to copy from 
		coordinateBufferSize,			// col-row size of chiBuffer 
		coordinateBufferCoordinate,		// top left src cell in chiBuffer 
		&writeRect);					// dest. screen buffer rectangle 
}

DWORD getInput(INPUT_RECORD **eventBuffer)
{
	DWORD numEvents = 0;
	DWORD numEventsRead = 0;

	HANDLE inputHandle = GetStdHandle(STD_INPUT_HANDLE);
	GetNumberOfConsoleInputEvents(inputHandle, &numEvents);
	if (numEvents) {
		*eventBuffer = reinterpret_cast<INPUT_RECORD *>(malloc(sizeof(INPUT_RECORD) * numEvents));
		ReadConsoleInput(inputHandle, *eventBuffer, numEvents, &numEventsRead);
	}

	return numEventsRead;
}

void Engine::gameLoop(Graph *graph)
{
	charBuffer = new CHAR_INFO[graph->vertexCount];
	killCount = 0;
	
	for (int index = 0; index < graph->vertexCount; index++) {
		switch (graph->vertices[index].category) {
		case PLAYER_SPAWN:
			playerSpawn = { graph->vertices[index].x, graph->vertices[index].y };
			break;
		case ENEMY_SPAWN:
		{
			PCOORD sp = new COORD();
			sp->X = graph->vertices[index].x;
			sp->Y = graph->vertices[index].y;
			enemySpawn.insert(sp);
			break;
		}
		default:
			break;
		}
	}

	// Configure player
	player = new View(playerSpawn, {2, 2});

	player->charInfo->Char.AsciiChar = static_cast<wchar_t>(64);
	player->charInfo->Attributes = BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_BLUE | BACKGROUND_INTENSITY | FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY;
	subviews.push_back(player);
	
	//COORD spawnPoint = *enemySpawns.begin();
	for (PCOORD sp : enemySpawn) {
		Enemy1 *enemyA = new Enemy1(this, *sp);
		enemies.push_back(enemyA);
	}

	/*spawnPoint.X = 6;
	spawnPoint.Y = 6;
	View *enemyB = new Enemy1(this, spawnPoint);
	subviews.insert(enemyB);*/


	gameOver = false;

	INPUT_RECORD *eventBuffer;
	int numberOfEvents = 0;

	while (!gameOver) {

		numberOfEvents = getInput(&eventBuffer);
		if (numberOfEvents) {
			for (int i = 0; i < numberOfEvents; i++) {
				if (eventBuffer[i].Event.KeyEvent.bKeyDown != 0) {
					continue;
				}
				processEventType(eventBuffer[i]);
			}
		}
		drawGraph(graph);
		Sleep(frameInterval); 
	}
}


void Engine::movePlayerToPosition(int x, int y)
{
	if (y < 0)
	{
		for (int i = 0; i < player->size.X; i++)
		{
			Vertex* v = &graph->vertices[(player->origin.Y + y) * graph->width + player->origin.X + i + x];

			if (v->category == WALL)
			{
				return;
			}
		}
		player->origin.X += x;
		player->origin.Y += y;
	}
	if (y > 0)
	{
		for (int i = 0; i < player->size.X; i++)
		{
			Vertex* v = &graph->vertices[(player->origin.Y + player->size.Y - 1 + y) * graph->width + player->origin.X + i + x];

			if (v->category == WALL)
			{
				return;
			}
		}
		player->origin.X += x;
		player->origin.Y += y;
	}
	if (x < 0)
	{
		for (int i = 0; i < player->size.Y; i++)
		{
			Vertex* v = &graph->vertices[(player->origin.Y + i  + y) * graph->width + player->origin.X + x];

			if (v->category == WALL)
			{
				return;
			}
		}
		player->origin.X += x;
		player->origin.Y += y;
	}
	if (x > 0)
	{
		for (int i = 0; i < player->size.Y; i++)
		{
			Vertex* v = &graph->vertices[(player->origin.Y + i + y) * graph->width + player->origin.X + player->size.X - 1 + x];

			if (v->category == WALL)
			{
				return;
			}
		}
		player->origin.X += x;
		player->origin.Y += y;
	}
	
}

void Engine::shootProjectile(COORD origin, COORD directon)
{
	Projectile* projectile = new Projectile();
	projectile->origin = origin;
	projectile->direction = directon;
	projectiles.push_back(projectile);
	PlaySound((LPCSTR) "C:/Windows/Media/ir_end", NULL, SND_FILENAME | SND_ASYNC);
}

bool projectileInBounds(SMALL_RECT window, const Projectile& projectile)
{
	return (projectile.origin.X >= 0 && projectile.origin.X <= window.Right && projectile.origin.Y >= 0 && projectile.origin.Y <= window.Bottom);
}

void Engine::processEventType(INPUT_RECORD inputRecord)
{
	switch (inputRecord.EventType) {
	case KEY_EVENT:
	{
		switch (inputRecord.Event.KeyEvent.wVirtualKeyCode)
		{
		case VK_ESCAPE:
			gameOver = true;
		case VK_DOWN:
		{
			movePlayerToPosition(0, 1);
			break;
		}
		case VK_UP:
		{
			movePlayerToPosition(0, -1);
			break;
		}
		case VK_RIGHT:
		{
			movePlayerToPosition(1, 0);
			break;
		}
		case VK_LEFT:
		{
			movePlayerToPosition(-1, 0);
			break;
		}
		case 0x57: //W
		{
			shootProjectile({ player->origin.X , player->origin.Y -2 }, { 0, -1 });
			break;
		}
		case 0x41: //A
		{
			shootProjectile({ player->origin.X -2 , player->origin.Y }, { -1, 0 });
			break;
		}
		case 0x53: //S
		{
			shootProjectile({ player->origin.X , player->origin.Y + player->size.Y + 1  }, { 0, 1 });
			break;
		}
		case 0x44: //D
		{
			shootProjectile({ player->origin.X + player->size.X + 1, player->origin.Y }, { 1, 0 });
			break;
		}
		}
	}
	case MOUSE_EVENT:
	{
		/*	if (eventBuffer[i].Event.MouseEvent.dwButtonState == FROM_LEFT_1ST_BUTTON_PRESSED) {
		player = { eventBuffer[i].Event.MouseEvent.dwMousePosition.X, eventBuffer[i].Event.MouseEvent.dwMousePosition.Y };
		drawGraph(graph);
		}*/
	}

	}
}