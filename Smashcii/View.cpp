#include "View.h"


View::View()
{
	origin = { 0, 0 };
	size = { 1, 1 };
	direction = { 0, 0 };
	charInfo = new CHAR_INFO;
	charInfo->Char.AsciiChar = static_cast<wchar_t>(0);
	disposable = false;
	destroy = false;
}

View::View(COORD origin, COORD size) : origin(origin), size(size), disposable(false)
{
	charInfo = new CHAR_INFO[size.X * size.Y];
	for (int charIndex = 0; charIndex < size.X * size.Y; charIndex++) {
		charInfo[charIndex].Char.AsciiChar = static_cast<wchar_t>(0);
	}
}

View::~View()
{
}

void View::update()
{

}