#pragma once

#include "View.h"

class Projectile : public View
{
public:
	Projectile();
	~Projectile();

	void update();
};

