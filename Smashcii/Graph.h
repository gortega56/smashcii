#pragma once
#include <string>
using namespace std;

class Vertex;

const int VERTEX_CAPACITY = 100;
const int EDGE_CAPACITY = 500;

class Graph
{

public:
	/*Vertex* vertices[VERTEX_CAPACITY];
	Edge* edges[EDGE_CAPACITY];*/
	
	Vertex* vertices;
	int vertexCount;
	int width, height;
	
	Graph();
	Graph(int width, int height);
	Graph(std::string LvlFile);
	~Graph();

	void addVertex(Vertex& vertex);
	string toString();
};

