#include "ReadLevel.h"

int * GetLevelWidthHeight(std::string LevelFile)
{
	// Returned Variables:
	int RetWidth;
	int RetHeight;
	int * RetWidthHeight = new int[2]; // Returned array

	// Open Level file in file stream
	std::ifstream LvlStream(LevelFile);

	if (LvlStream.is_open())
	{
		// Read First Line
		std::string FirstLine;
		getline(LvlStream, FirstLine);

		// Separate Width and Height in two different variables
		std::stringstream WidthHeightStream(FirstLine);
		std::string WidthString;
		std::string HeightString;
		std::getline(WidthHeightStream, WidthString, ' ');	// Read Height
		std::getline(WidthHeightStream, HeightString);		// Read Width

		// String 2 Integer Conversion 
		RetHeight = std::stoi(HeightString, nullptr);
		RetWidth = std::stoi(WidthString, nullptr);
		
		// Preparing Return Array
		RetWidthHeight[0] = RetWidth;
		RetWidthHeight[1] = RetHeight;
		return RetWidthHeight;
	}
	else
	{
		// ERROR: File not found.
		std::cout << "Unable to open Level File" << std::endl << std::endl;
		return nullptr;
	}
}

std::vector<std::string> ReadLevel(std::string LevelFile)
{
	// Returned Variable
	std::vector<std::string> LvlVector;
	// Open Level file in file stream
	std::ifstream LvlStream(LevelFile);
	//Stores each retrieve line before putting them on the LevelVector
	std::string LvlString;

	if (LvlStream.is_open())
	{
		// Skips First Line (Width and Height)
		getline(LvlStream, LvlString);

		// Reads through the rest of the file
		while (getline(LvlStream, LvlString))
		{
			LvlVector.push_back(LvlString);
		}
	}
	else
	{
		// ERROR: File not found.
		std::cout << "Unable to open Level File" << std::endl << std::endl;
	}
	// Return LvlVector.
	return LvlVector;
	
}

	