#pragma once
#include <set>
#include <string>
#include <stack>

using namespace std;

const int ORTHOGONAL_MOVEMENT_COST = 10;
const int DIAGONAL_MOVEMENT_COST = 14;

class Vertex;

class Node
{
public:
	Vertex* vertex;
	Node* parent;
	int F, G, H;
	bool isObstacle;

	Node(){};
	Node(const Node& other) : vertex(other.vertex), parent(other.parent), F(other.F), G(other.G), H(other.H) {}
	Node(Vertex *vertex) : vertex(vertex), parent(__nullptr), F(0), G(0), H(0) {}
	Node(Vertex *vertex, Node *parent) : vertex(vertex), parent(parent) {}
	string toString();
};

struct NodeCompare2
{
	bool operator() (Node* node1, Node* node2) {
		return (node1->toString() < node2->toString());
	}
};

class AStar
{

private:

	set<Node *, NodeCompare2> openNodeSet;
	set<Node *, NodeCompare2> closedNodeSet;
	const Node* currentNode;

	void addOpenNodeForVertex(Vertex *vertex);
	void getNextNode();

	void addAdjacentNodes(Node *node);

	set<Vertex *> closedVertices();

	Node* findOrCreateNodeByVertex(Vertex *vertex);
	void updateNodeForDestination(Node *node, Vertex *destination);
	stack<Node*> outputPath();
public:
	AStar();
	~AStar();
	stack<Node*> findPath(Vertex* const source, Vertex* const destination);
};



