#include "Graph.h"
#include "Vertex.h"
#include <iostream>
#include <Windows.h>
#include <string>
#include "Engine.h"
#include "ReadLevel.h"
#include "GameDirector.h"

using namespace std;

const int  CONSOLE_WIDTH = 239;
const int CONSOLE_HEIGHT = 82; 

int main()
{	
	bool play = true;

	do {
		GameDirector Director = GameDirector(8);

		Director.LoadRandomLvl();
		//Director.LoadLvl(8);
		Director.StartGameLoop();

		system("cls");
		cout << "Score: " << Director.Score<<endl;
		cout << "Continue?" << endl;
		char response;
		cin >> response;
		play = (response == 'Y' || response == 'y');
	} while (play); 

return 0;

}